"use strict";
// tokens.js
// 2010-02-23

// Produce an array of simple token objects from a string.
// A simple token object contains these members:
//      type: 'name', 'string', 'number', 'operator'
//      value: string or number value of the token
//      from: index of first character of the token
//      to: index of the last character + 1

// Comments are ignored.

// Debe estar cada elemento detr�s del anterior. No pueden haber
// elementos en medio sin ser casados
RegExp.prototype.bexec = function(str) {
  var i = this.lastIndex;
  var m = this.exec(str);
  if (m && (m.index == i)) return m;
  return null;
}

String.prototype.tokens = function () {
    var from;                   // The index of the start of the token.
    var i = 0;                  // The index of the current character.
    var n;                      // The number value.
    var str;                    // The string value.
    var m;                      // Matching
    var result = [];            // An array to hold the results.

    var WHITES              = /\s+/g;																// = ___;
    var ID                  = /[a-zA-Z]+(\d)*\w*\b/g;										// = ___;
    var NUM                 = /[-+]?\d+(.\d+)?/g;										// = ___;
    var STRING              = /("(\\.|[^"])*")|('(\\.|[^'])*')/g;		// = ___;
    var ONELINECOMMENT      = /\/\/[^\n]*/g;												// = ___;
    var MULTIPLELINECOMMENT = /\/[*].*?[*]\//g;											// = ___;
    var TWOCHAROPERATORS    = /([+][+]|[-][-]|[+]=|[-]=|[*]=|\/=|%=|&=|[|]=|==|&&|[|][|]|<<|>>|!=|<=|>=|^=|<<=|>>=)/g;		// = ___;
    var ONECHAROPERATORS    = /[-+*/%=&|^!~<>?]/g;									// = ___;
		var PUNCTUATORS 				= /[{}\[\]().,;:]/g;

    // Make a token object.
    var make = function (type, value) {
        return {
            type: type,			// type: ___,		// tipo de token
            value: value,		// value: ___,		// valor del token
            from: from,			// from: ___,		// variable from (donde comenzó el token)
            to: i						// to: ___		// variable i	(índice de la siguiente cadena)
        };
    };

    // Begin tokenization. If the source string is empty, return nothing.
    if (!this) return;			// this: cadena  
		
		//alert("cadena: "+this+", cadena.length: "+this.length);
    // Loop through this text
		while (i < this.length) {
				WHITES.lastIndex =  ID.lastIndex = NUM.lastIndex = STRING.lastIndex =	// NUM.lastIndex = __.lastIndex =
        ONELINECOMMENT.lastIndex = MULTIPLELINECOMMENT.lastIndex =		// __.lastIndex = __.lastIndex = 
        TWOCHAROPERATORS.lastIndex = ONECHAROPERATORS.lastIndex = 
				PUNCTUATORS.lastIndex = i;		// __.lastIndex = __.lastIndex = ___;

        from = i;		// donde acab�
        // Ignore whitespace.
        if (m = WHITES.bexec(this)) {
            str = m[0];
            i = WHITES.lastIndex;																// ___
						//alert ("ES BLANCO. i=lastIndex= "+i);
        // name.
        } else if (m = ID.bexec(this)) {
            str = m[0];
						//alert("Es ID: "+str);
            i = ID.lastIndex;																		// ____
            result.push(make('name', str));

        // number.
        } else if (m = NUM.bexec(this)) {
            str = m[0];
						//alert("Es n�mero: "+str);
            i = NUM.lastIndex;																	// ___

            n = +str;
            if (isFinite(n)) {
                result.push(make('number', n));									// result.___(make('number', n));
            } else {
                make('number', str).error("Bad number");
            }
        // string
        } else if (m = STRING.bexec(this)) {
            str = m[0];
						//alert("Es cadena: "+str);
            i = STRING.lastIndex;																// ___
            str = str.replace(/^["']/,'');											// /^___/
            str = str.replace(/["']$/,'');
            result.push(make('string', str));										// result.___(make('string', str));
        // comment.
        } else if ((m = ONELINECOMMENT.bexec(this))  || 
                   (m = MULTIPLELINECOMMENT.bexec(this))) {
            str = m[0];
						//alert("Es comentario: "+str+", i: "+i+"ONELINECOMMENT.lastIndex: "+ONELINECOMMENT.lastIndex+"MULTIPLELINECOMMENT.lastIndex: "+MULTIPLELINECOMMENT.lastIndex);
						if (ONELINECOMMENT.lastIndex != 0){			// cas�
            	i = ONELINECOMMENT.lastIndex;											// ___
						} else {
							i = MULTIPLELINECOMMENT.lastIndex;
						}
        // two char operator
        } else if (m = TWOCHAROPERATORS.bexec(this)) {
            str = m[0];
						//alert("Es dos operadores: "+str);
            i = TWOCHAROPERATORS.lastIndex;																			// ___
            result.push(make('operator', str));									// result.___(make(
        // single-character operator						
        } else if (m = ONECHAROPERATORS.bexec(this)){
						//alert("Es un operador: "+m[0]);
            result.push(make('operator', this.substr(i,1)));		// result.___(make('
            i = ONECHAROPERATORS.lastIndex;																			// ___
        } else if (m = PUNCTUATORS.bexec(this)) {
						str = m[0];
						//alert("Es PUNCTUATOR: "+str);
            i = PUNCTUATORS.lastIndex;													// ____
            result.push(make('name', str));
				} else {
          throw "Syntax error near '"+this.substr(i,1)+"'";
        }
				if (i==0){	// Lleg� al final de la cadena
					break;		// Sale del while
				}
    }
    return result;
};

